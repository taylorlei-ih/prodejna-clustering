<?php   
    require 'class/ClustrovacNode.php';
    require 'class/ClustrovacProduct.php';
    
    error_reporting(E_ALL); ini_set('display_errors', 1);
    ini_set('memory_limit','1000M');
    set_time_limit(60);
    
    const DEV_NODE_LIMIT = 5000;
    const DEV_RELATIONS_LIMIT = 25;
    
    const DATA_FOLDER = 'data/';
    const CSV_DELIMETER = ',';
    
    const SALES_FILE       = 'pbt2.csv';
    const PRECLUSTERS_FILE = 'bc_series.csv';
    const SIMILARITY_FILE  = 'bbts1.csv';
    const METADATA_FILE    = 'pp1.csv';
    const WHITELIST_FILE   = 'whitelist_final.csv';
    
    $load = false;
    $clustering = false;
    //nejdriv zkusime naloadovat
    if (isset($_GET['load'])) {
        $load = true;
        
        //pri loadovani se musi setnout tick
        $saveData = file_get_contents(DATA_FOLDER.$_GET['load']);
        $json = json_decode($saveData);
        $tick = (int) $json->tick;
        $jsonString = $json->data; // data
        $clustering = $json->clustering;
        
        foreach ($clustering->nodeLabels as $node => $label) {
            if (!$label) {
                unset($clustering->nodeLabels[$node]);
            }
        }
    } 
    
    //pokud failne load, tak se taky zacina od zacatku
    if (!isset($_GET['load']) || empty($jsonString)) {
        $load = false;
        
        //predavane promenne
        $maxNodeId            = 0;
        $nodes                = array();
        $nodeRelationsTotal   = array();
        $nodeRelationsAverage = array();
        $productNodeReference = array();
        $nodeRelationsTotalProductCount = array();
        
        //pomocne promenne
        $whitelist         = array();
        $bookRelations     = array();
        $productSales      = array();
        $productCategories = array();
        $productAuthors    = array();
        
        //WHITELIST
        $currentMimoEuromedie = 0;
        $maxMimoEuromedie = 250;
        $whitelistFile = fopen(DATA_FOLDER.WHITELIST_FILE, 'r');
        while (($line = fgetcsv($whitelistFile, 0, CSV_DELIMETER)) !== false) {
            $id_product = (int) $line[0];
            $dodavetele = explode('_', $line[1]);
            
            $isEuromedie = in_array('26', $dodavetele);
            
            if ($id_product && ($isEuromedie || $currentMimoEuromedie < $maxMimoEuromedie)) {
                $whitelist[$id_product] = true;
                if (!$isEuromedie) {
                    $currentMimoEuromedie++;
                }
            }
        }
        fclose($whitelistFile);
        
        //NEJPRVE NALOADUJEME METADATA
        //ziskame prodejnost knih
        $salesFile = fopen(DATA_FOLDER.SALES_FILE, 'r');
        while (($line = fgetcsv($salesFile, 0, CSV_DELIMETER)) !== false) {
            $id_product   = (int) $line[0];
            $bought_times = (int) $line[1];
                
            if ($id_product && $bought_times) {
                $productSales[$id_product] = $bought_times;
            }
        }
        fclose($salesFile);
        
        //ziskame kategorie a autory knih
        $metadataFile = fopen(DATA_FOLDER.METADATA_FILE, 'r');
        while (($line = fgetcsv($metadataFile, 0, CSV_DELIMETER)) !== false) {
            $id_product          = (int) $line[0];
            $id_category_default = (int) $line[1];
            $id_supplier         = (int) $line[2];
            
            if (!$id_category_default) {
                $id_category_default = 1;
            }
            if (!$id_supplier) {
                $id_supplier = 1;
            }
            
            if ($id_product && $id_category_default && $id_supplier) {
                $productCategories[$id_product] = $id_category_default;
                $productAuthors[$id_product]    = $id_supplier;
            }
        }
        fclose($metadataFile);

        //JAKO DRUHE SE NALOUDUJI SERIE 2x
        for ($iteration = 0; $iteration < 2; $iteration++) {
            $preclustersFile = fopen(DATA_FOLDER.PRECLUSTERS_FILE, 'r');
            while (($line = fgetcsv($preclustersFile, 0, CSV_DELIMETER)) !== false) {
                $id_node  = (int) $line[0];
                $id_product = (int) $line[1];

                if ($id_node > $maxNodeId) {
                    $maxNodeId = $id_node;
                }

                //chceme jen ceske knizky
                if (!isset($productNodeReference[$id_product]) && $id_node && $id_product && isset($productSales[$id_product]) 
                    && (isset($whitelist[$id_product]) || (isset($nodes[$id_node]) && $productSales[$id_product] > 50))
                    && $id_product < 1000000 && count($nodes) < DEV_NODE_LIMIT
                ) { //DEV PODMINKA
                    if (!isset($nodes[$id_node])) {
                        $nodes[$id_node] = new ClustrovacNode($id_node);
                    }

                    $productNodeReference[$id_product] = $id_node;

                    $nodes[$id_node]->product_ids[] = $id_product;
                    $nodes[$id_node]->total_sales  += $productSales[$id_product];

                    $nodes[$id_node]->product_metadata[$id_product] = new ClustrovacProduct(
                        $id_product,
                        (isset($productCategories[$id_product]) ? $productCategories[$id_product] : 1), 
                        (isset($productAuthors[$id_product]) ? $productAuthors[$id_product] : 1),
                        $productSales[$id_product]
                    );
                }
            }
            fclose($preclustersFile);
        }
        
        //JAKO POSLEDNI LOADUJEME SIMILARITY, KTERE UZ PRIRADIME SAMOTNYM NODES
        $similarityFile = fopen(DATA_FOLDER.SIMILARITY_FILE, 'r');
        while (($line = fgetcsv($similarityFile, 0, CSV_DELIMETER)) !== false) {
            $id_product1     = (int) $line[0];
            $id_product2     = (int) $line[1];
            //POKUS SE SCORE
            $bought_together = (int) $line[2];
            $score           = (int) $line[3];
            
            $usedParamater = $score;
            if ($usedParamater < 20) {
                continue;
            }
            //vytvoreni vlastniho node pro hodne prodejne knihy
            foreach (array($id_product1, $id_product2) as $id_product) {
                if ((isset($whitelist[$id_product]) || (isset($productSales[$id_product]) && $productSales[$id_product] > 50)) && !isset($productNodeReference[$id_product]) 
                    && $id_product < 1000000 
                    && count($nodes) < DEV_NODE_LIMIT
                ) { //DEV PODMINKA
                    $id_node = $maxNodeId + 1;
                    $maxNodeId++;

                    $nodes[$id_node] = new ClustrovacNode($id_node);
                    $productNodeReference[$id_product] = $id_node;
                    $nodes[$id_node]->product_ids[] = $id_product;
                    $nodes[$id_node]->total_sales  += $productSales[$id_product];
                    $nodes[$id_node]->product_metadata[$id_product] = new ClustrovacProduct(
                        $id_product,
                        $productCategories[$id_product], 
                        $productAuthors[$id_product],
                        $productSales[$id_product]
                    );
                }
            }
            
            //prirazeni spolecne kupovanosti
            if (isset($productNodeReference[$id_product1]) && isset($productNodeReference[$id_product2]) && isset($whitelist[$id_product1]) && isset($whitelist[$id_product2])) {
                //obe knihy jsou v preclusterech -> muzeme s nimi pracovat
                $node_id1 = $productNodeReference[$id_product1];
                $node_id2 = $productNodeReference[$id_product2];
                if ($node_id1 === $node_id2) {
                    continue;
                }
                
                $nodeRelationReferenceString = min($node_id1, $node_id2).'_'.max($node_id1, $node_id2);
                if (isset($nodes[$node_id1])) {
                    $nodes[$node_id1]->relations[] = $node_id2;
                }
                if (isset($nodes[$node_id2])) {
                    $nodes[$node_id2]->relations[] = $node_id1;
                }
                if (isset($nodeRelationsTotal[$nodeRelationReferenceString])) {
                    $nodeRelationsTotal[$nodeRelationReferenceString] += $usedParamater;
                    $nodeRelationsTotalProductCount[$nodeRelationReferenceString] += 1;
                } else {
                    $nodeRelationsTotal[$nodeRelationReferenceString] = $usedParamater;
                    $nodeRelationsTotalProductCount[$nodeRelationReferenceString] = 1;
                }
            }
        }
        fclose($similarityFile);

        //NA KONCI PRO DOKONCENI SERII
        $preclustersFile = fopen(DATA_FOLDER.PRECLUSTERS_FILE, 'r');
        while (($line = fgetcsv($preclustersFile, 0, CSV_DELIMETER)) !== false) {
            $id_node  = (int) $line[0];
            $id_product = (int) $line[1];

            if ($id_node > $maxNodeId) {
                $maxNodeId = $id_node;
            }

            //chceme jen ceske knizky
            if (!isset($productNodeReference[$id_product]) && $id_node && $id_product && isset($productSales[$id_product]) 
                && (isset($whitelist[$id_product]) || (isset($nodes[$id_node])))
                && $id_product < 1000000
            ) { //DEV PODMINKA
                if (!isset($nodes[$id_node])) {
                    $nodes[$id_node] = new ClustrovacNode($id_node);
                }

                $productNodeReference[$id_product] = $id_node;

                $nodes[$id_node]->product_ids[] = $id_product;
                $nodes[$id_node]->total_sales  += $productSales[$id_product];

                $nodes[$id_node]->product_metadata[$id_product] = new ClustrovacProduct(
                    $id_product,
                    (isset($productCategories[$id_product]) ? $productCategories[$id_product] : 1), 
                    (isset($productAuthors[$id_product]) ? $productAuthors[$id_product] : 1),
                    $productSales[$id_product]
                );
            }
        }
        fclose($preclustersFile);
        
        //RECALCULATE SCORES AND AVERAGE SALES
        foreach ($nodeRelationsTotal as $key => $usedParamater) {
            $nodeRelationsAverage[$key] = $usedParamater / $nodeRelationsTotalProductCount[$key];
        }
        foreach ($nodes as $key => $node) {
            $node->calcAverageSales();
            $relationsSum = 0;
            foreach ($node->relations as $relKey => $relNode) {
                if (!isset($nodes[$relNode])) {
                    unset($node->relations[$relKey]);
                } else {
                    $nodeRelationReferenceString = min($key, $relNode).'_'.max($key, $relNode);
                    $relationsSum += $nodeRelationsTotal[$nodeRelationReferenceString];
                }
            }            
            
            $node->relations = array_values(array_unique($node->relations));
            if (empty($node->relations) || $relationsSum < 40) {
                unset($nodes[$key]);
            }
        }
        
        //VYPIS PRO JS
        $json = array(
            'nodes' => $nodes,
            'node_ids' => array_keys($nodes),
            'node_relations_total' => $nodeRelationsTotal,
            'node_relations_average' => $nodeRelationsAverage,
            'product_node_reference' => $productNodeReference
        );
        $jsonString = $json;
        $tick = 0;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Clustrovač pro prodejnu</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="clustrovac.css">
    </head>
    <body>
        <canvas id="universe"></canvas>
        <a id="downloadAnchorElem" style="display:none"></a>
        <a id="downloadCSVAnchorElem" download="product_ids.csv" style="display:none"></a>
        <div id="controlsContainer">
            <table id="controls">
                <tr><td><button id="beginBtn" colspan="2">Začít!</button></td></tr>
                <tr><td><button id="beginClusteringBtn" colspan="2">Provést clustrování!</button></td></tr>
                <tr><td><button id="correctClusteringBtn" colspan="2">Uprav clustrování!</button></td></tr>
                <tr><td><button id="showClusteringBtn" colspan="2">Vizualizuj vsechny clustery!</button></td></tr>
                <tr><td><button id="fitClustersBtn" colspan="2">Hrubě nafitovat!</button></td></tr>
                <tr><td><button id="exportBtn" colspan="2">Exportovat</button></td></tr>
                <tr><td><button id="editorSkriniButton" colspan="2">Editor Skrini</button></td></tr>
            </table>
        </div>
        <div id="posControlsContainer">
            <table id="controls">
                <tr><td></td><td><button id="upBtn">&#129045;</button></td><td></td>
                    <td><button id="biggerBtn">+</button></td></tr>
                <tr><td><button id="leftBtn">&#129044;</button></td><td><button id="resetPosBtn">&#10227;</button></td><td><button id="rightBtn">&#129046;</button></td>
                    <td><button id="resetScaleBtn">&#10227;</button></td></tr>
                <tr><td></td><td><button id="downBtn">&#129047;</button></td><td></td>
                    <td><button id="smallerBtn">-</button></td></tr>
            </table>
        </div>
        
        <div id="clusterVisualiser">
            cluster_id: <input type="text" id="clusterVisualiserIdInput"> <button>Ukaz!</button>
            <div></div>
        </div>
        
        <div id="allClusterVisualiser">
            <button>X</button>
            <div></div>
        </div>
        
        <div id="editorSkrini">
            <div id="editorLeftMenu">
                <button class="closeBtn">X</button> <button id="editorSkriniExportBtn" style="float: right">Export</button><br>
                <input type="text" id="editorSkriniImportInput" value="e_final-1-11-2017"> <button id="editorSkriniImportBtn">Import</button><br>
                <button id="editorSkriniRedistributeLimboBtn">Redistribute Limbo</button><br>
                <button id="editorSkriniExportSkrineAsListBtn">Export Skrine</button> <button id="editorSkriniExportPyramidyAsListBtn">Export Pyramidy</button>
                <table>
                    <tr><th>&#8627;</th><th>Posun</th><th>Cislo</th><th>Typ</th><th>Nazev</th><th>Kapacita</th></tr>
                </table>
            </div>
            <div id="editorLocationView">
                <div id="editorLocationViewHeader">Nazev: <input type="text" id="editorSeriiName"> <span id="activeCapacity"></span> <button id="moveEntireActiveBtn">&#8644;</button></div>
                <div id="editorLocationContent">
                    <h2>Aktivni</h2>
                    <div id="editorLocationContentActive">
                    </div>
                    <h2>Neaktivni</h2>
                    <div id="editorLocationContentInactive">
                    </div>
                </div>
            </div>
        </div>
        
        <div id="tickContainer">Tick: <input type="text" id="tickElem"></div>
        <div id="debugContainer"></div>
        <div id="nahled" class="hidden"></div>
        
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
        <script src="jquery-1.11.1.min.js"></script>
        <script src="clustrovac.js"></script>
        <script>
            var jsonString = <?php echo json_encode($jsonString); ?>;
            var json = jsonString;
            var clustrovac = new Clustrovac(json, document.getElementById('universe'), <?php echo ($load ? 'true' : 'false'); ?>, <?php echo $tick; ?> <?php echo ($clustering ? ', '.json_encode($clustering) : ''); ?>);
        </script>
    </body>
</html>