function getBaseLog(x, y) {
  return Math.log(y) / Math.log(x);
};

var Clustrovac = function(data, canvas, load, tick, clustering) {
    this.data = data;
    this.clustering = clustering || {
        clusters: [],
        clusterSize: [],
        nodeLabels: [],
        shown: null,
        positioning: [],
        positioned: [],
        clusterSimilarity: [],
        clustersWithSize: 0,
        forExport: {
            pyramidy: [],
            skrine: [],
            limbo: []
        }
    };
    this.editorSkrini = null;
    
    this.clusteringStage = 0;
    if (clustering) {
        this.clusteringStage = 2;
    }
    this.forces = [];
    this.load = load || false;

    this.currentNahled = [];

    //konstanty
    this.logicTick = tick || 0;
    this.const = {
        maxlogpersec: 120,
        maxrendpersec: 2,
        initalSpread: 200,
        speedBoost: 3,
        rScale: 1,
        
        repulsiveForceBegin: 240,
        repulsiveForce2Begin: 8000000,
        repulsiveConst: 36,
        repulsiveConst2: 10,
        repulsiveForce2Dist: 3,
        minRepulsiveDist: 1,
        
        similarityConst: 150000,
        
        velocityRoof2Begin: 280,
        velocityRoof: 180,
        velocityRoof2: 5,
        
        changePositionValue: 50,
        circleScale: 3,
        lineScale: 1,
        renderLineMinimumScore: 50,
        radiusNahledShow: 25,
        
        clMinPts: 4,
        clEpsilon: 35,
        clEpsilonMax: 48,
        clCorrectionDelta: 10,
        clCorrectionMaxIter: 12,
        
        clFitPyramidSize: 200,
        clFitPyramidCountX: 6,
        clFitPyramidCountY: 2,
        clFitDisplayWindowSize: 30,
        clFitDisplayWindowCount: 26,
        clFitDisplayWindowRatio: 90
    };
    this.constNames = {
        maxlogpersec:"Logic per sec",
        maxrendpersec: "Frames per sec",
        initalSpread: "Rozptyl",
        speedBoost: "Zrychlení",
        rScale: "Škála pro r",
        
        repulsiveForceBegin: "Počátek odpuzování",
        repulsiveForce2Begin: "Počátek lokál. megaodpuzování",
        repulsiveConst: "Odpudivá konst (setiny)",
        repulsiveConst2: "Odpudivá konst 2 (setiny)",
        repulsiveForce2Dist: "Vzdál. působení odp. síly 2",
        minRepulsiveDist: "Min. vzdál. odpudivosti",
        
        similarityConst: "Přitažlivá konst (setiny)",
        
        velocityRoof2Begin: "Počátek Max. rychlost 2",
        velocityRoof: "Max. rychlost (tis)",
        velocityRoof2: "Max. rychlost 2 (tis)",
        
        changePositionValue: "Posun",
        circleScale: "Velikost kruhu",
        lineScale: "Šířka spojů",
        renderLineMinimumScore: "Min. hodnota spoje k renderování",
        radiusNahledShow: "Tolerance R náhledu",
        
        clMinPts: "Clustering minPts",
        clEpsilon: "Clustering ε min (start)",
        clEpsilonMax: "Clustering ε max",
        clCorrectionDelta: "Clustering correction delta",
        clCorrectionMaxIter: "Clustering max iterations",
        
        clFitPyramidSize: "Knih v pyramide",
        clFitDisplayWindowSize: "Knih ve skrini",
        clFitDisplayWindowRatio: "Procento knih obalem",
        clFitPyramidCountX: "Pyramidy x",
        clFitPyramidCountY: "Pyramidy y",
        clFitDisplayWindowCount: "Pocet skrini",
    };

    //ovladani
    this.ctrl = {
        drawLines: true,
        showCategoryId: false,
        showProductId: false,
        showClusterId: false,
        doRendering: true,
        doLogic: true,
        doNahled: false,
    };
    this.ctrlNames = {
        drawLines: "Renderuj spoje",
        showCategoryId: "Ukaž ID kategorie",
        showProductId: "Ukaž ID produktu",
        showClusterId: "Ukaž ID clusteru",
        doRendering: "Zapnout renderování",
        doLogic: "Zapnout logiku",
        doNahled: "Zapnout náhled"
    };
    
    this.camera = {
        x: 0,
        y: 0
    };
    this.mouse = {
        x: 0,
        y: 0
    };
    this.scale = 50;

    this.ctx = canvas.getContext('2d');
    
    this.init();
    
    if (load) {
        $('#beginBtn').click();
        this.ctrl.doLogic = false;
        $('.control-elem input[data-ctrl="doLogic"]').prop('checked', false);
    }
};

Clustrovac.prototype.init = function() {
    this.ctx.canvas.width = window.innerWidth;
    this.ctx.canvas.height = window.innerHeight;
    this.ctx.translate(this.ctx.canvas.width/2, this.ctx.canvas.height/2);
    this.ctx.scale(this.scale, this.scale);
    
    var that = this;
    for (var constant in this.const) {
        var constvalue = this.const[constant];
        var constname = this.constNames[constant];
        
        var $controlElem = $('<tr/>', {'class': 'control-elem'});
        var $input = $('<input type="number" value="'+constvalue+'" data-const="'+constant+'">');
        $input.on('input', function(e) {
            var constantChanged = $(this).attr('data-const');
            if ($(this).val() > 0) {
                that.const[constantChanged] = parseInt($(this).val());
            }
        });
        var $name = '<td>'+constname+'</td>';
        $controlElem.append($name);
        $controlElem.append($('<td/>').append($input));
        $('#controls').append($controlElem);
    }
    
    for (var control in this.ctrl) {
        var controlvalue = this.ctrl[control];
        var controlname = this.ctrlNames[control];
        
        var checked = '';
        if (controlvalue) {
            checked = "checked";
        }
        
        var $controlElem = $('<tr/>', {'class': 'control-elem'});
        var $input = $('<input type="checkbox" data-ctrl="'+control+'" '+checked+'>');
        $input.on('click', function(e) {
            var controlChanged = $(this).attr('data-ctrl');
            if ($(this).prop('checked')) {
                that.ctrl[controlChanged] = true;
            } else {
                that.ctrl[controlChanged] = false;
            }   
        });
        var $name = '<td>'+controlname+'</td>';
        $controlElem.append($name);
        $controlElem.append($('<td/>').append($input));
        $('#controls').append($controlElem);
    }
    
    $('#beginBtn').on('click', function() {
        if (!$(this).prop('disabled')) {
            that.begin();
            console.log('initiated');
        }
        $(this).prop('disabled', true);
    });
    
    $('#beginClusteringBtn').on('click', function() {
        that.doLogic = false;
        that.clusteringStage = true;
        var origEpsilon = that.const.clEpsilon;
        $(this).prop('.control-elem input[data-ctrl="doLogic"]', false);
        
        var clusterCount = 0;
        var lessClusters = false;
        while (that.const.clEpsilon < that.const.clEpsilonMax) {
            that.doDBSCAN(!lessClusters);
            
            clusterCountNew = that.clustering.clusters.length;
            if (clusterCount > clusterCountNew && !lessClusters) {
                lessClusters = true;
                that.const.clEpsilon--;
                that.doDBSCAN(true);
                that.const.clEpsilon++;
                //that.const.clMinPts--;
                $('.control-elem input[data-const="clEpsilon"]').val(that.const.clEpsilon);
                $('.control-elem input[data-const="clMinPts"]').val(that.const.clMinPts);
                clusterCountNew = that.clustering.clusters.length;
            } else if (clusterCount <= clusterCountNew && that.const.clEpsilon < that.const.clEpsilonMax) {
                that.const.clEpsilon++;
                $('.control-elem input[data-const="clEpsilon"]').val(that.const.clEpsilon);
            } else {
                that.const.clEpsilon = origEpsilon;
                $('.control-elem input[data-const="clEpsilon"]').val(origEpsilon);
                break;
            }
            clusterCount = clusterCountNew;
            that.clustering.clustersWithSize = clusterCount;
        }
    });
    
    $('#showClusteringBtn').on('click', function() {
        if (that.clusteringStage < 2) {
            return;
        }
        
        that.visualiseAllClusters();
        $('#allClusterVisualiser').show();
    });
    
    $('#allClusterVisualiser > button').on('click', function() {
        $('#allClusterVisualiser').hide();
    });
    
    $('#fitClustersBtn').on('click', function() {
        that.fitClustersToShop();
    });
    
    $('#editorSkriniButton').on('click', function() {
        if (that.clusteringStage < 2) {
            return;
        }
        
        var exportData = {
            pyramidy: [],
            skrine: [],
            limbo: []
        };
        
        var i, j;
        //pyramidy
        for (i = 0; i < that.clustering.forExport.pyramidy.length; i++) {
            var pyramidaProduct_ids = [];
            var cluster_ids = that.clustering.forExport.pyramidy[i];
            
            for (j = 0; j < that.data.node_ids.length; j++) {
                var node_id = that.data.node_ids[j];
                var nodeLabel = that.clustering.nodeLabels[node_id];
                if (cluster_ids.indexOf(nodeLabel) >= 0) {
                    var node = that.data.nodes[node_id];
                    pyramidaProduct_ids = pyramidaProduct_ids.concat(node.product_ids);
                }
            }
            
            exportData.pyramidy.push(pyramidaProduct_ids);
        }
        
        //skrine
        for (i = 0; i < that.clustering.forExport.skrine.length; i++) {
            var skrinProduct_ids = [];
            var cluster_ids = that.clustering.forExport.skrine[i];

            for (j = 0; j < that.data.node_ids.length; j++) {
                var node_id = that.data.node_ids[j];
                var nodeLabel = that.clustering.nodeLabels[node_id];
                if (cluster_ids.indexOf(nodeLabel) >= 0) {
                    var node = that.data.nodes[node_id];
                    skrinProduct_ids = skrinProduct_ids.concat(node.product_ids);
                }
            }
            
            exportData.skrine.push(skrinProduct_ids);
        }
        
        if (that.clustering.forExport.skrine.length < that.const.clFitDisplayWindowCount) {
            for (i = 0; i < that.const.clFitDisplayWindowCount - that.clustering.forExport.skrine.length; i++) {
                exportData.skrine.push([]);
            }
        }
        
        //limbo 
        if (that.clustering.forExport.limbo) {
            var limboProduct_ids = [];
            var cluster_ids = that.clustering.forExport.limbo;

            for (j = 0; j < that.data.node_ids.length; j++) {
                var node_id = that.data.node_ids[j];
                var nodeLabel = that.clustering.nodeLabels[node_id];
                if (cluster_ids.indexOf(nodeLabel) >= 0) {
                    var node = that.data.nodes[node_id];
                    limboProduct_ids = limboProduct_ids.concat(node.product_ids);
                }
            }

            exportData.limbo[0] = limboProduct_ids;
        }
        that.editorSkrini = new EditorSkrini($('#editorSkriniButton'), exportData, that);
        
        that.ctrl.doLogic = false;
        $('.control-elem input[data-ctrl="doLogic"]').prop('checked', false);
        
        that.ctrl.doRendering = false;
        $('.control-elem input[data-ctrl="doRendering"]').prop('checked', false);
        
        $('#editorSkrini').show();
        that.editorSkrini.renderActive();
    });
    
    $('#editorLeftMenu > .closeBtn').on('click', function() {
        $('#editorSkrini').hide();
    });
    
    $('#exportBtn').on('click', function() {
        var dataExport = {
            data: that.data,
            clustering: that.clustering,
            tick: that.logicTick
        };
        
        var url = URL.createObjectURL( new Blob( [JSON.stringify(dataExport)], {type:'data:text/json;charset=utf-8'} ) );
        $("#downloadAnchorElem").attr("href", url)[0].click();
    });
    
    $('#correctClusteringBtn').on('click', function() {
        that.clusterCorrectionLoop();
    });
    
    $('#clusterVisualiser > button').on('click', function() {
        if (that.clusteringStage < 2) {
            return;
        }
        
        $('#clusterVisualiser > div').html('');
        var cluster_id = parseInt($('#clusterVisualiserIdInput').val());
        var product_ids = [];
        for (var node_id in that.clustering.nodeLabels) {
            if (that.clustering.nodeLabels[node_id] !== null && that.clustering.nodeLabels[node_id] === cluster_id) {
                var nodeProduct_ids = that.data.nodes[node_id].product_ids;
                product_ids = product_ids.concat(nodeProduct_ids);
            }
        }
        
        var pi;
        for (pi = 0; pi < product_ids.length; pi++) {
            var id_product = product_ids[pi];
            var id_node = that.data.product_node_reference[id_product];
            $('#clusterVisualiser > div').append('<div style="display: inline-block;">'+id_product+'<br>('+id_node+', c:'+that.clustering.nodeLabels[id_node]+')<br><img style="height: 100px;" src="https://img-cloud.megaknihy.cz/'+id_product+'-small/0/img.jpg"><div>');
        }        
        
        that.clustering.shown = cluster_id;
    });
    
    $('#upBtn').on('click', function() {
        that.ctx.translate(0, that.const.changePositionValue/that.scale);
        that.camera.y += that.const.changePositionValue/that.scale;
    });
    $('#downBtn').on('click', function() {
        that.ctx.translate(0, -that.const.changePositionValue/that.scale);
        that.camera.y -= that.const.changePositionValue/that.scale;
    });
    $('#leftBtn').on('click', function() {
        that.ctx.translate(that.const.changePositionValue/that.scale, 0);
        that.camera.x += that.const.changePositionValue/that.scale;
    });
    $('#rightBtn').on('click', function() {
        that.ctx.translate(-that.const.changePositionValue/that.scale, 0);
        that.camera.x -= that.const.changePositionValue/that.scale;
    });
    $('#resetPosBtn').on('click', function() {
        that.ctx.translate(-that.camera.x, -that.camera.y);
        that.camera.x = 0;
        that.camera.y = 0;
    });
    
    $('#smallerBtn').on('click', function() {
        that.ctx.translate(-that.camera.x, -that.camera.y);
        that.ctx.scale(1/that.scale, 1/that.scale);
        that.scale *= 0.8;
        that.ctx.scale(that.scale, that.scale);
        that.ctx.translate(that.camera.x, that.camera.y);
    });
    $('#biggerBtn').on('click', function() {
        that.ctx.translate(-that.camera.x, -that.camera.y);
        that.ctx.scale(1/that.scale, 1/that.scale);
        that.scale *= 1.2;
        that.ctx.scale(that.scale, that.scale);
        that.ctx.translate(that.camera.x, that.camera.y);
    });
    $('#resetScaleBtn').on('click', function() {
        that.ctx.translate(-that.camera.x, -that.camera.y);
       that.ctx.scale(1/that.scale, 1/that.scale);
       that.scale = 6;
       that.ctx.scale(that.scale, that.scale);
       that.ctx.translate(that.camera.x, that.camera.y);
    });
    
    $('#universe').on('mousemove', function(e) {
       that.mouse.x = -that.camera.x + (event.clientX - that.ctx.canvas.width/2)/that.scale;
       that.mouse.y = -that.camera.y + (event.clientY - that.ctx.canvas.height/2)/that.scale;
       
       if (that.ctrl.doNahled && that.logicTick > 0) {
           $('#nahled').css({'left': event.clientX, 'top': event.clientY+30});
           
            var mx = that.mouse.x;
            var my = that.mouse.y;
            var expectedDelta = that.const.radiusNahledShow*2/that.scale;

            that.currentNahled = [];
            for (var nodeid in that.data.nodes) {
                var node = that.data.nodes[nodeid];
                var dx = Math.abs(node.pos.x - mx);
                var dy = Math.abs(node.pos.y - my);
                if (dx < expectedDelta && dy < expectedDelta) {
                    var dr = Math.sqrt(dx*dx + dy*dy);
                    if (dr < that.const.radiusNahledShow/that.scale) {
                        that.currentNahled.push(parseInt(nodeid));
                    }
                }
            }

            that.drawNahled();
        } else {
            $('#nahled').addClass('hidden');
        }
    });
    
    $("body").on("keydown", function(e) {
        switch(e.which) {
            case 37:
                that.ctx.translate(that.const.changePositionValue/that.scale, 0);
                that.camera.x += that.const.changePositionValue/that.scale;
                break;
            case 38:
                that.ctx.translate(0, that.const.changePositionValue/that.scale);
                that.camera.y += that.const.changePositionValue/that.scale;
                break;
            case 39:
                that.ctx.translate(-that.const.changePositionValue/that.scale, 0);
                that.camera.x -= that.const.changePositionValue/that.scale;
                break;
            case 40:
                that.ctx.translate(0, -that.const.changePositionValue/that.scale);
                that.camera.y -= that.const.changePositionValue/that.scale;
                break;
            case 109:
                that.ctx.translate(-that.camera.x, -that.camera.y);
                that.ctx.scale(1/that.scale, 1/that.scale);
                that.scale *= 0.8;
                that.ctx.scale(that.scale, that.scale);
                that.ctx.translate(that.camera.x, that.camera.y);
                break;
            case 107:
                that.ctx.translate(-that.camera.x, -that.camera.y);
                that.ctx.scale(1/that.scale, 1/that.scale);
                that.scale *= 1.2;
                that.ctx.scale(that.scale, that.scale);
                that.ctx.translate(that.camera.x, that.camera.y);
                break;
        }
    });
    
    var i;
    var productCount = 0;
    for (i = 0; i < this.data.node_ids.length; i++) {
        productCount += this.data.nodes[this.data.node_ids[i]].product_ids.length;
    }
    console.log("Products in nodes: "+productCount);
};

Clustrovac.prototype.begin = function() {
    var seed = 1256;
    var seedCents = Math.floor(seed / 100);
    var seedModulo = Math.floor(seed / 100);
    var seedSubtract = (3000000 % seed) / seed * 25;
    
    if (this.load === false) {
        for (var i = 0; i < this.data.node_ids.length; i++) {
            var node = this.data.nodes[this.data.node_ids[i]];
            var firstProductCategory = parseInt(node.product_metadata[node.product_ids[0]].id_category);
            if (firstProductCategory < 1500) {
                firstProductCategory = 1502;
            }
            var cents = Math.floor((firstProductCategory -1500)/ 25);
            var modulo = firstProductCategory % 100;
            var seedPos = ((node.id_node % seed) / seed) * 50 - seedSubtract;
            
            /*node.pos = {
                x: seedPos + (cents - 10)/20 * this.const.initalSpread,
                y: Math.abs(seedPos)-100 + (modulo - 50)/100 * this.const.initalSpread
            };*/
            node.pos = {
                x: (Math.random() - 0.5) * this.const.initalSpread,
                y: (Math.random() - 0.5) * this.const.initalSpread
            };
            node.force = {
                x: 0,
                y: 0
            };
        }
    } else {
        this.ctrl.doLogic = false;
    }
    
    this.loopLogic();
    this.loopRender();
};

Clustrovac.prototype.loopLogic = function() {
    this.logic();
    $('#tickElem').val(this.logicTick);
    setTimeout(this.loopLogic.bind(this), 1000/this.const.maxlogpersec);
};
Clustrovac.prototype.loopRender = function() {
    var d1 = new Date();
    this.render();
    var d2 = new Date();
    var frameTime = d2.getTime()-d1.getTime();
    var potentialFPS = Math.floor(1000/(d2.getTime()-d1.getTime()));
    $('#debugContainer').html('fps: '+Math.min(potentialFPS, this.const.maxrendpersec)+' ('+potentialFPS+')' );
    setTimeout(this.loopRender.bind(this), 1000/this.const.maxrendpersec - frameTime);
};

Clustrovac.prototype.logic = function() {
    if (this.ctrl.doLogic === false) {
        return;
    }
    
    this.logicTick++;

    for (var i = 0; i < this.data.node_ids.length; i++) {
        var node = this.data.nodes[this.data.node_ids[i]];
        var relations = node.relations;
        
        for (var k = 0; k < this.data.node_ids.length; k++) {
            if (k === i) {
                continue;
            }
            this.calculateRepulsiveForce(this.data.node_ids[i], this.data.node_ids[k]);
            
            if(this.logicTick > this.const.repulsiveForce2Begin) {
                this.calculateRepulsiveForce2(this.data.node_ids[i], this.data.node_ids[k]);
            }
        }

        for (var j = 0; j < relations.length; j++) {
            this.calculateSimilarityForce(this.data.node_ids[i], relations[j]);
        }
    }
    
    var maxVelocity = this.const.velocityRoof*this.const.speedBoost/1000;
    if (this.logicTick > this.const.velocityRoof2Begin) {
        maxVelocity = this.const.velocityRoof2*this.const.speedBoost/1000;
    }
    
    for (var i = 0; i < this.data.node_ids.length; i++) {
        var node = this.data.nodes[this.data.node_ids[i]];    
        
        var accelX = node.force.x/node.average_sales;
        var accelY = node.force.y/node.average_sales;
        
        if (accelX > maxVelocity) {
            accelY = maxVelocity/accelX * accelY;
            accelX = maxVelocity;
        } else if (accelX < -maxVelocity) {
            accelY = -maxVelocity/accelX * accelY;
            accelX = -maxVelocity;
        }
        
        if (accelY > maxVelocity) {
            accelX = maxVelocity/accelY * accelX;
            accelY = maxVelocity;
        } else if (accelY < -maxVelocity) {
            accelX = -maxVelocity/accelY * accelX;
            accelY = -maxVelocity;
        }
        
        node.pos.x += accelX;
        node.pos.y += accelY;
        
        node.force.x = 0;
        node.force.y = 0;
    }
};
Clustrovac.prototype.render = function() {
    if (this.ctrl.doRendering === false) {
        return;
    }
    
    var ctx= this.ctx;
    var canvasWidth = this.ctx.canvas.width/this.scale;
    var canvasHeight = this.ctx.canvas.height/this.scale;
    var ctxX = -canvasWidth/2 - this.camera.x;
    var ctxY = -canvasHeight/2 - this.camera.y;
    ctx.clearRect(ctxX, ctxY, canvasWidth, canvasHeight);
    //lines
    if (this.ctrl.drawLines) {
        for (var rels in this.data.node_relations_total) {
            var scoreObj = this.data.node_relations_average[rels];
            if (scoreObj < this.const.renderLineMinimumScore) {
                continue;
            }
            
            var splitted = rels.split('_');
            if(splitted[0] == 0 || splitted[1] == 0) {
                continue;
            }

            var b1 = this.data.nodes[splitted[0]];
            var b2 = this.data.nodes[splitted[1]];
            if (!this.relVisible(b1, b2)) {
                continue;
            }

            ctx.lineWidth = Math.min(getBaseLog(10, scoreObj)/150*this.const.lineScale, 0.2);
            if (this.currentNahled.length > 0 && (this.currentNahled.indexOf(parseInt(splitted[0])) >= 0 || this.currentNahled.indexOf(parseInt(splitted[1])) >= 0)) {
                ctx.lineWidth *= 5;
            }
            var opacity = Math.max(scoreObj/50, 0.3);
            if (opacity < 0.4) {
                opacity = 0.4;
            }
            
            ctx.beginPath();
            ctx.moveTo(b1.pos.x, b1.pos.y);
            ctx.lineTo(b2.pos.x, b2.pos.y);
            ctx.strokeStyle = "rgba(0,0,0, "+opacity/4+")";
            if (opacity > 1) {
                ctx.strokeStyle = "rgba(60,0,220, "+(opacity-0.5)/2+")";
            }
            if (opacity > 3) {
                ctx.strokeStyle = "rgba(60,225,0, "+(opacity-1.5)/2+")";
            }
            if (opacity > 6) {
                ctx.strokeStyle = "rgba(255,0,0, 1)";
            }   
            ctx.stroke();
            ctx.closePath();
        }
    }
    
    for (var i = 0; i < this.data.node_ids.length; i++) {
        var node_id = this.data.node_ids[i];
        var node = this.data.nodes[node_id];
        if (this.pointInView(node)) {
            ctx.beginPath();
            var category = node.product_metadata[node.product_ids[0]].id_category;
            var catnormalised = Math.floor(((category - 1500)/300) * 255 * 255 * 255);

            ctx.arc(node.pos.x, node.pos.y, getBaseLog(1.2, node.average_sales)/1500*this.const.circleScale, 0, 2 * Math.PI);
            ctx.closePath();
            ctx.fillStyle = "#"+catnormalised.toString(16);
            if (this.clusteringStage === 2) {
                var cluster = this.clustering.nodeLabels[node_id];
                if (cluster !== null) {
                    var gBase = this.clustering.clusters.length/3;
                    var cModulo = cluster % Math.floor(gBase);
                    var cCycle = Math.floor(cluster/gBase);
                    var h = Math.round(180 * cModulo/gBase);
                    ctx.fillStyle = "hsl("+h+", "+Math.floor(100*gBase/3)+"%, 80%)";
                    ctx.strokeStyle = "#111111";
                    ctx.lineWidth = 1/1000*this.const.circleScale;
                    
                    if (this.clustering.shown === cluster) {
                        ctx.strokeStyle = "#000000";
                        ctx.lineWidth *= 5;
                    }
                    ctx.stroke();
                } else {
                    ctx.fillStyle = "gray";
                }
            }            
            ctx.fill();
            if (this.currentNahled.length > 0 && this.currentNahled.indexOf(node_id) >= 0) {
                ctx.strokeStyle = "#111111";
                ctx.lineWidth = 1/1000*this.const.circleScale;
                ctx.stroke();
            }
        }
     }
    
    for (var i = 0; i < this.data.node_ids.length; i++) {
        var offset = 0;
        var node_id = this.data.node_ids[i];
        
        if (this.ctrl.showCategoryId) {
            var node = this.data.nodes[node_id];
            ctx.textAlign="center"; 
            ctx.textBaseline="middle"; 
            ctx.font="1px monospace";
            var category = node.product_metadata[node.product_ids[0]].id_category;
            ctx.fillText(category, node.pos.x, node.pos.y);  
            ctx.fillStyle = "black";
            ctx.fill();
            ctx.strokeStyle = "white";
            ctx.stroke();
            ctx.closePath();
            offset += 1;
        }

        if (this.ctrl.showProductId) {
            var node = this.data.nodes[node_id];
            ctx.textAlign="center"; 
            ctx.textBaseline="middle"; 
            ctx.font="1px monospace";
            ctx.fillText(node_id, node.pos.x, node.pos.y+offset);  
            ctx.fillStyle = "black";
            ctx.fill();
            ctx.strokeStyle = "white";
            ctx.stroke();
            ctx.closePath();
        }
        
        if (this.ctrl.showClusterId && this.clusteringStage === 2 && this.clustering.nodeLabels[node_id] !== null) {
            var node = this.data.nodes[this.data.node_ids[i]];
            ctx.textAlign="center"; 
            ctx.textBaseline="middle"; 
            ctx.font="0.5px Arial";
            ctx.fillText(this.clustering.nodeLabels[node_id], node.pos.x, node.pos.y+offset);  
            ctx.fillStyle = "black";
            ctx.fill();
            ctx.strokeStyle = "white";
            ctx.stroke();
            ctx.closePath();
        }
    }
    
    ctx.beginPath();
    ctx.fillStyle = "magenta";
    ctx.fillRect(0, 0, 1, 1);
    ctx.closePath();
    
    ctx.beginPath();
    ctx.fillStyle = "rgba(0,255,0,0.3)";
    ctx.arc(this.mouse.x, this.mouse.y, this.const.radiusNahledShow/this.scale, 0, 2 * Math.PI);
    ctx.fill();
    ctx.closePath();
};

Clustrovac.prototype.calculateRepulsiveForce = function(b1id, b2id) {
    var gravkonst = this.const.repulsiveConst/100*this.const.speedBoost;
    if (this.logicTick < this.const.repulsiveForceBegin) {
        return;
    }

    var b1 =  this.data.nodes[b1id];
    var b2 =  this.data.nodes[b2id];

    var x = Math.abs(b1.pos.x - b2.pos.x);
    var y = Math.abs(b1.pos.y - b2.pos.y);
    if (x > 8 || y > 8) {
        return;
    }
    
    var r = Math.max(Math.sqrt( Math.pow(x, 2) + Math.pow(y, 2)), this.const.minRepulsiveDist)/this.const.rScale;
    
    var mass = b1.average_sales * b2.average_sales;
    mass = 100000;
    var relId = Math.min(b1id, b2id)+"_"+Math.max(b1id, b2id);
    if (this.data.node_relations_average.hasOwnProperty(relId) && this.data.node_relations_average[relId] > 100) {
        mass = 800;
    } else if (this.data.node_relations_average.hasOwnProperty(relId)&& this.data.node_relations_average[relId] > 20)  {
        mass = mass / Math.log(this.data.node_relations_average[relId]);
    }
    
    var force = gravkonst * (mass / (r*r));
    if (isNaN(force)) {
        return;
    }
    
    var forceValueX = force * x/r;
    var forceValueY = force * y/r;
    if (b2.pos.x > b1.pos.x) {
        b1.force.x -= forceValueX;
        b2.force.x += forceValueX;
    } else {
        b1.force.x += forceValueX;
        b2.force.x -= forceValueX;
    }
    
    if (b2.pos.y > b1.pos.y) {
        b1.force.y -= forceValueY;
        b2.force.y += forceValueY;
    } else {
        b1.force.y += forceValueY;
        b2.force.y -= forceValueY;
    }
};

Clustrovac.prototype.calculateRepulsiveForce2 = function(b1id, b2id) {
    var gravkonst = this.const.repulsiveConst2/100*this.const.speedBoost;
    
    var b1 =  this.data.nodes[b1id];
    var b2 =  this.data.nodes[b2id];

    var x = Math.abs(b1.pos.x - b2.pos.x);
    var y = Math.abs(b1.pos.y - b2.pos.y);
    if (x > 4 || y > 4) {
        return;
    }
    
    var r = Math.max(Math.sqrt( Math.pow(x, 2) + Math.pow(y, 2)), this.const.minRepulsiveDist)/this.const.rScale;
        
    if (r > this.const.repulsiveForce2Dist) {
        return;
    }
    
    var force = gravkonst * (b1.average_sales * b2.average_sales / (r*r));

    if (b2.pos.x > b1.pos.x) {
        b1.force.x -= force * x/r;
        b2.force.x += force * x/r;
    } else {
        b1.force.x += force * x/r;
        b2.force.x -= force * x/r;
    }
    
    if (b2.pos.y > b1.pos.y) {
        b1.force.y -= force * y/r;
        b2.force.y += force * y/r;
    } else {
        b1.force.y += force * y/r;
        b2.force.y -= force * y/r;
    }
};

Clustrovac.prototype.calculateSimilarityForce = function(b1id, b2id) {
    var similaritykonst = this.const.similarityConst/100*this.const.speedBoost;
    
    var b1 =  this.data.nodes[b1id];
    var b2 =  this.data.nodes[b2id];

    if (!b1 || !b2 || !b1.pos.x || !b2.pos.x || !b1.pos.y || !b2.pos.y) {
        return;
    }
    
    var x = Math.abs(b1.pos.x - b2.pos.x);
    var y = Math.abs(b1.pos.y - b2.pos.y);
    
    var r = Math.sqrt( Math.pow(x, 2) + Math.pow(y, 2));
    
    var l = 1;
    l = r/this.const.rScale;
    
    var relId = Math.min(b1id, b2id)+'_'+Math.max(b1id, b2id);
    var force = -l*l*similaritykonst*(this.data.node_relations_average[relId]);
    
    //experiment
    if (this.data.node_relations_total[relId]/this.data.node_relations_average[relId] > 5) {
        force *= 2;
    }

    var fx = force/r;
    var fy = force/r;
    
    if (b2.pos.x > b1.pos.x) {
        b1.force.x -= fx * x;
        b2.force.x += fx * x;
    } else {
        b1.force.x += fx * x;
        b2.force.x -= fx * x;
    }
    
    if (b2.pos.y > b1.pos.y) {
        b1.force.y -= fy * y;
        b2.force.y += fy * y;
    } else {
        b1.force.y += fy * y;
        b2.force.y -= fy * y;
    }
};

Clustrovac.prototype.drawNahled = function() {
    if (this.currentNahled.length === 0) {
        $('#nahled').html('');
        $('#nahled').addClass('hidden');
        return;
    }
    
    $('#nahled').html('Produktů: ' + this.currentNahled.length + '<br>');
    for (i = 0; i < this.currentNahled.length; i++) {
        var requestedProductId = this.data.nodes[this.currentNahled[i]].product_ids[0];
        this.buildNahledElem(requestedProductId);
    }
    
    $('#nahled').removeClass('hidden');
};

Clustrovac.prototype.buildNahledElem = function(id_product) {
    var requestedNahledElemId = '#nahled-'+id_product;
    
    $('#nahled').append('<div style="display: inline-block;">'+id_product+'<br><img id="'+requestedNahledElemId+'" src="https://img-cloud.megaknihy.cz/'+id_product+'-small/0/img.jpg"><div>');
};

Clustrovac.prototype.relVisible = function(b1, b2) {
    if (!b1 || !b2) {
        return false;
    }
    
    var canvasWidth = this.ctx.canvas.width/this.scale;
    var canvasHeight = this.ctx.canvas.height/this.scale;
    var ctxX = -canvasWidth/2 - this.camera.x;
    var ctxY = -canvasHeight/2 - this.camera.y;

    if (
        (Math.min(b1.pos.x, b2.pos.x) < ctxX && Math.max(b1.pos.x, b2.pos.x) < ctxX) ||
        (Math.min(b1.pos.x, b2.pos.x) > ctxX + canvasWidth && Math.max(b1.pos.x, b2.pos.x) > ctxX + canvasWidth) ||
        (Math.min(b1.pos.y, b2.pos.y) < ctxY && Math.max(b1.pos.y, b2.pos.y) < ctxY) ||
        (Math.min(b1.pos.y, b2.pos.y) > ctxY + canvasHeight && Math.max(b1.pos.y, b2.pos.y) > ctxY + canvasHeight)
    ) {
        return false;
    } else {
        return true;
    }
};

Clustrovac.prototype.pointInView = function(node) {
    var canvasWidth = this.ctx.canvas.width/this.scale;
    var canvasHeight = this.ctx.canvas.height/this.scale;
    var ctxX = -canvasWidth/2 - this.camera.x;
    var ctxY = -canvasHeight/2 - this.camera.y;

    return (
        node.pos.x <= ctxX + canvasWidth && node.pos.x >= ctxX &&
        node.pos.y <= ctxY + canvasHeight && node.pos.y >= ctxY
    );
};

Clustrovac.prototype.doDBSCAN = function(reset) {
    if (reset){
        this.clustering.clusters = [];
        this.clustering.nodeLabels = [];
    }
    
    var ni;
    for (ni = 0; ni < this.data.node_ids.length; ni++) {
        var id_node = this.data.node_ids[ni];
        var node = this.data.nodes[id_node];
        
        //previously processed
        if ((reset && this.clustering.nodeLabels.hasOwnProperty(id_node)) || (!reset && this.clustering.nodeLabels[id_node] !== null)) {
            continue;
        }
        
        //get neighbours
        var neighbouringNodeIds = this.doDBSCANNeighbours(node);

        //is noise?
        if (neighbouringNodeIds.length < this.const.clMinPts) {
            //null is noise
            this.clustering.nodeLabels[id_node] = null; 
            continue;
        }
        //not noise
        var clusterId = this.clustering.clusters.length;
        this.clustering.clusters.push(clusterId);
        this.clustering.nodeLabels[id_node] = clusterId;
        
        var seed = neighbouringNodeIds;
        var si;
        for (si = 0; si < seed.length; si++) {
            var seedNode_id = seed[si];
            var seedNode = this.data.nodes[seedNode_id];
            if (this.clustering.nodeLabels.hasOwnProperty(seedNode_id) && this.clustering.nodeLabels[seedNode_id] === null) {
                this.clustering.nodeLabels[seedNode_id] = clusterId;
            } else if (this.clustering.nodeLabels.hasOwnProperty(seedNode_id)) {
                continue;
            } else if (!this.clustering.nodeLabels.hasOwnProperty(seedNode_id)) {
                this.clustering.nodeLabels[seedNode_id] = clusterId;    
                var seedNeighbouringNodeIds = this.doDBSCANNeighbours(seedNode);
                
                if (seedNeighbouringNodeIds.length >= this.const.clMinPts) {
                    seed = seed.concat(seedNeighbouringNodeIds);
                }
            }
        }
    }
    
    var nodesInClusters = 0;
    var productsInClusters = 0;
    //napocitani produktu v clusteru
    for (var node_id in this.clustering.nodeLabels) {
        var cluster_id = this.clustering.nodeLabels[node_id];
        if (cluster_id !== null) {
            nodesInClusters++;
            productsInClusters += this.data.nodes[node_id].product_ids.length;
        }
    }
    
    if (reset) {
        console.log("Reset, Nodes in clusters: " + nodesInClusters + " (" + productsInClusters + " products); NoC: "+this.clustering.clusters.length);
    } else {
        console.log("No reset, Nodes in clusters: " + nodesInClusters + " (" + productsInClusters + " products); NoC: "+this.clustering.clusters.length);
    }
    //clustering finished
    this.clusteringStage = 2;
    this.updateClusterSize();
};

Clustrovac.prototype.doDBSCANNeighbours = function(referenceNode) {   
    var refX = referenceNode.pos.x;
    var refY = referenceNode.pos.y;
    var neighbours = [];
    
    var nj;
    for (nj = 0; nj < this.data.node_ids.length; nj++) {
        var id_nodeTested = this.data.node_ids[nj];
        var nodeTested = this.data.nodes[id_nodeTested];

        var testedX = nodeTested.pos.x;
        var testedY = nodeTested.pos.y;
        
        var dx = Math.abs(testedX - refX);
        var dy = Math.abs(testedY - refY);
        
        if (dx <= this.const.clEpsilon/100 && dy <= this.const.clEpsilon/100 && this.clustering.nodeLabels[id_nodeTested] === null) {
            var dr = Math.sqrt(dx*dx + dy*dy);
            
            if (dr <= this.const.clEpsilon/100) {
                neighbours.push(id_nodeTested);
            }
        }
    }
    
    return neighbours;
};

Clustrovac.prototype.clusterCorrectionLoop = function() {
    var iterations = 0;
    var max_iterations = this.const.clCorrectionMaxIter;
    
    var correctedDestinationBuffer;
    do {
        correctedDestinationBuffer = [];
        //loop through nodes in clusters
        for (var node_id in this.clustering.nodeLabels) {
            var node = this.data.nodes[node_id];
            var nodeCluster_id = this.clustering.nodeLabels[node_id];
            
            //in first iteration assign clusters to nodes without clusters
            if (nodeCluster_id !== null && iterations === 0) {
                continue;
            }
            
            var nodeAltClusters = [];
            var nodeAltClustersRelsCount = [];

            //loop through relations for node
            var ri;
            for (ri = 0; ri < node.relations.length; ri++) {
                //add scores to different clusters
                var targetNode_id = node.relations[ri];
                
                if (!targetNode_id || !this.clustering.nodeLabels.hasOwnProperty(targetNode_id) || !this.clustering.nodeLabels[targetNode_id] || isNaN(this.clustering.nodeLabels[targetNode_id])) {
                    //not clustered
                    continue;
                }
                
                var targetNodeCluster_id = this.clustering.nodeLabels[targetNode_id];
                var relation_key = Math.min(node_id, targetNode_id) + "_" + Math.max(node_id, targetNode_id);
                var relationScore = this.data.node_relations_average[relation_key];

                //only relevant
                if (relationScore < 15 || !targetNodeCluster_id || isNaN(targetNodeCluster_id) || targetNodeCluster_id === "NaN") {
                    continue;
                }
                
                if (!nodeAltClusters.hasOwnProperty(targetNodeCluster_id)) {
                    nodeAltClusters[targetNodeCluster_id] = relationScore;
                    nodeAltClustersRelsCount[targetNodeCluster_id] = 1;
                } else {
                    nodeAltClusters[targetNodeCluster_id] += relationScore;
                    nodeAltClustersRelsCount[targetNodeCluster_id]++;
                }
            }

            //test for alternate clusters
            var largestScoreCluster_id = null;
            var largestScore = 0;
            var originalScore = 0;
            var testedClusterScore = null;
            for (var testedCluster_id in nodeAltClusters) {
                //average scores
                testedClusterScore = nodeAltClusters[testedCluster_id]/nodeAltClustersRelsCount[targetNodeCluster_id];
                if (testedClusterScore > largestScore && this.clustering.clusterSize[testedCluster_id]) {
                    largestScore = testedClusterScore;
                    largestScoreCluster_id = testedCluster_id;
                }
                
                if (testedCluster_id == nodeCluster_id) {
                    originalScore = testedClusterScore;
                }
            }
            
            //put command to change cluster
            if (parseInt(largestScoreCluster_id) !== parseInt(nodeCluster_id) && largestScoreCluster_id !== null 
                && largestScore >= Math.max((Math.log10(1+this.clustering.clusterSize[largestScoreCluster_id])-0.3), 1.1)*originalScore
                && this.clustering.clusterSize[largestScoreCluster_id] < 100
            ) {
                correctedDestinationBuffer.push({
                    node_id: node_id,
                    destinationCluster_id: largestScoreCluster_id,
                    lcs: largestScore,
                    ocs: originalScore
                });
            }
            
            //new experimental reassign
            /*if (correctedDestinationBuffer && correctedDestinationBuffer[0]) {
                node_id = parseInt(correctedDestinationBuffer[0].node_id);
                destinationCluster_id = parseInt(correctedDestinationBuffer[0].destinationCluster_id);

                this.clustering.nodeLabels[node_id] = destinationCluster_id;
            }*/
        }
        
        //old place to reassign
        
        //reassign
        var bi, node_id, destinationCluster_id;
        for (bi = 0; bi < correctedDestinationBuffer.length; bi++) {
            node_id = parseInt(correctedDestinationBuffer[bi].node_id);
            destinationCluster_id = parseInt(correctedDestinationBuffer[bi].destinationCluster_id);
            
            this.clustering.nodeLabels[node_id] = destinationCluster_id;
        }
        console.log(correctedDestinationBuffer);
        console.log("Cluster correction - corrected "+correctedDestinationBuffer.length+" nodes in iteration "+iterations);
        iterations++;
        
       this.updateClusterSize();
    } while (correctedDestinationBuffer.length > this.const.clCorrectionDelta && iterations < max_iterations);
    
    console.log("Cluster correction took "+iterations+" iterations");
};

Clustrovac.prototype.visualiseAllClusters = function() {
    $('#allClusterVisualiser > div').html('');
    
    var ci;
    for (ci = 0; ci < this.clustering.clusters.length; ci++) {
        $('#allClusterVisualiser > div').append("<div id='cl-"+this.clustering.clusters[ci]+"'><h1>Cluster "+this.clustering.clusters[ci]+"</h1></div>");
    }
    
    var product_ids = [];
    for (var id_node in this.clustering.nodeLabels) {
        var nodeProduct_ids = this.data.nodes[id_node].product_ids;
        product_ids = product_ids.concat(nodeProduct_ids);
    }
        
    var pi;
    for (pi = 0; pi < product_ids.length; pi++) {
        var id_product = product_ids[pi];
        var id_node = this.data.product_node_reference[id_product];
        var id_cluster = this.clustering.nodeLabels[id_node];
        $('#cl-'+id_cluster).append('<div style="display: inline-block;">'+id_product+'<br>('+id_node+', c:'+this.clustering.nodeLabels[id_node]+')<br><img style="height: 100px;" src="https://img-cloud.megaknihy.cz/'+id_product+'-small/0/img.jpg"><div>');
    }
};

Clustrovac.prototype.updateClusterSize = function() {
    var ci;
    var clustersWithSize = [];
    for (ci = 0; ci < this.clustering.clusters.length; ci++) {
        this.clustering.clusterSize[this.clustering.clusters[ci]] = 0;
    }
    
    for (var id_node in this.clustering.nodeLabels) {
        var nodeProduct_ids = this.data.nodes[id_node].product_ids;
        var id_cluster = this.clustering.nodeLabels[id_node];
        if (id_cluster) {
            this.clustering.clusterSize[id_cluster] += nodeProduct_ids.length;
            
            if (clustersWithSize.indexOf(id_cluster) === -1) {
                clustersWithSize.push(id_cluster);
            }
        }
    }
    
    var total = this.clustering.clusterSize.reduce((sum, value) => sum + value, 1);
    this.clustering.clustersWithSize = clustersWithSize.length;
    console.log("Products in clusters "+total);
};

Clustrovac.prototype.fitClustersToShop = function() {
    var x = 0;
    var y = 0;
    var n = 0;
    var overflow = false;
    var currentSize = 0;
    var lastCluster = [];
    var secondToLastCluster = [];
    
    //pyramidy
    var k = 0;
    var servingDisplayWindows = false;
    var dumpingInLimbo = false;
    for (var j = 0; j < this.clustering.clustersWithSize + 1; j++) {
        if (overflow) {
            if (!dumpingInLimbo){
                if (k < this.const.clFitPyramidCountX * this.const.clFitPyramidCountY - 1) {
                    if (k % 2 === 0) {
                        if (y === 0) {
                            y = 1;
                        } else {
                            y = 0;
                        }
                    } else {
                        x++;
                    }

                    k++;
                } else {
                    if (!servingDisplayWindows) {
                        secondToLastCluster = null;
                        lastCluster = null;
                    }
                    servingDisplayWindows = true;
                    n++;

                    if (n > this.const.clFitDisplayWindowCount) {
                        dumpingInLimbo = true;
                        servingDisplayWindows = false;
                    }
                }
                overflow = false;
                secondToLastCluster = lastCluster;
                lastCluster = [];
                currentSize = 0;
            }
            if (j === this.clustering.clustersWithSize) {
                break;
            }
        } 
        
        var cluster_id;
        if (dumpingInLimbo || (this.clustering.positioned.length === 0 || (servingDisplayWindows && n === 0 && !secondToLastCluster))) {
            cluster_id = this.getLargestRemainingCluster();
            secondToLastCluster.push(cluster_id);
        } else if (x === 0 && y <= 1 || (servingDisplayWindows && n <= 1 && secondToLastCluster)) {
            cluster_id = this.findClosestRemainingCluster(secondToLastCluster);
            if (!dumpingInLimbo && !cluster_id && lastCluster.length > 0) {
                overflow = true;
                continue;
            }
            
            if (!cluster_id) {
                cluster_id = this.getLargestRemainingCluster();
                console.log('No similar clusters, had to choose largest cluster');
            }
            lastCluster.push(cluster_id);
        } else {
            if (lastCluster) {
                cluster_id = this.findClosestRemainingCluster(lastCluster);
            } else {
                cluster_id = this.findClosestRemainingCluster(secondToLastCluster);
            }
            
            var newClusterSize = this.clustering.clusterSize[cluster_id];
            if (!dumpingInLimbo && !cluster_id && lastCluster.length > 0 || 
                (currentSize + newClusterSize >= this.const.clFitPyramidSize + 10 || (servingDisplayWindows && currentSize + newClusterSize>= this.const.clFitDisplayWindowSize + 10))
            ) {
                overflow = true;
                continue;
            }
            
            if (!cluster_id) {
                cluster_id = this.getLargestRemainingCluster();
                console.log('No similar clusters, had to choose largest cluster');
            }
            lastCluster.push(cluster_id);
        }
        
        this.clustering.positioned.push(cluster_id);
        if (!servingDisplayWindows && !dumpingInLimbo) {
            this.clustering.positioning.push({
                cluster_id: cluster_id,
                type: "pyramid",
                x: x,
                y: y
            });
            
            if (this.clustering.forExport.pyramidy.length < k+1) {
                this.clustering.forExport.pyramidy.push([cluster_id]);
            } else {
                this.clustering.forExport.pyramidy[k].push(cluster_id);
            }
        } else if (servingDisplayWindows) {
            this.clustering.positioning.push({
                cluster_id: cluster_id,
                type: "skrin",
                n: n
            });
            
            if (this.clustering.forExport.skrine.length < n+1) {
                this.clustering.forExport.skrine.push([cluster_id]);
            } else {
                this.clustering.forExport.skrine[n].push(cluster_id);
            }
        } else if (dumpingInLimbo) {
            this.clustering.positioning.push({
                cluster_id: cluster_id,
                type: "limbo",
                l: 0
            });
            
            this.clustering.forExport.limbo.push(cluster_id);
        }
        
        currentSize += this.clustering.clusterSize[cluster_id];
        if (!servingDisplayWindows && !dumpingInLimbo) {
            console.log (j+'-th iteration, k: '+k, 'x:'+x, 'y:'+y, currentSize, cluster_id);
        } else if (servingDisplayWindows) {
            console.log (j+'-th iteration', 'n:'+n, currentSize, cluster_id);
        } else {
            console.log (j+'-th iteration', 'limbo', currentSize, cluster_id);
        }
        
        if (!dumpingInLimbo && (currentSize >= this.const.clFitPyramidSize || (servingDisplayWindows && currentSize >= this.const.clFitDisplayWindowSize * 2))) {
            overflow = true;
            currentSize = 0;
        }
    }
    
    while (this.clustering.positioned.length < this.clustering.clustersWithSize) {
        var cluster_id = this.getLargestRemainingCluster();
        this.clustering.positioned.push(cluster_id);
        this.clustering.forExport.limbo.push(cluster_id);
    }
};

Clustrovac.prototype.findClosestRemainingCluster = function(cluster_ids) {
    var nodesInCluster = [];
    for (var id_node in this.clustering.nodeLabels) {
        if (cluster_ids.indexOf(this.clustering.nodeLabels[id_node]) >= 0) {
            nodesInCluster.push(id_node);
        }
    }
    
    var ni;
    var similarClusters = [], similarClustersRelsCount = [];
    for (ni = 0; ni < nodesInCluster.length; ni++) {
        var node_id = parseInt(nodesInCluster[ni]);
        var node = this.data.nodes[node_id];
        var nodeCluster_id = this.clustering.nodeLabels[node_id];
        if (cluster_ids.indexOf(nodeCluster_id) === -1) {
            continue;
        }
        var ri;
        for (ri = 0; ri < node.relations.length; ri++) {
            //add scores to different clusters
            var targetNode_id = node.relations[ri];
            //console.log(targetNode_id);
            if (!targetNode_id || !this.clustering.nodeLabels.hasOwnProperty(targetNode_id) || !this.clustering.nodeLabels[targetNode_id] || isNaN(this.clustering.nodeLabels[targetNode_id])) {
                //not clustered
                continue;
            }

            var targetNodeCluster_id = this.clustering.nodeLabels[targetNode_id];
            var relation_key = Math.min(node_id, targetNode_id) + "_" + Math.max(node_id, targetNode_id);
            var relationScore = this.data.node_relations_average[relation_key];

            //only relevant
            if (relationScore < 15 || !targetNodeCluster_id || isNaN(targetNodeCluster_id) || targetNodeCluster_id === "NaN") {
                continue;
            }

            if (!similarClusters.hasOwnProperty(targetNodeCluster_id)) {
                similarClusters[targetNodeCluster_id] = relationScore;
                similarClustersRelsCount[targetNodeCluster_id] = 1;
            } else {
                similarClusters[targetNodeCluster_id] += relationScore;
                similarClustersRelsCount[targetNodeCluster_id]++;
            }
        }
    }
    //console.log(similarClusters, cluster_ids);
    //test for alternate clusters
    var largestScoreCluster_id = null;
    var largestScore = 0;
    var testedClusterScore = null;
    for (var testedCluster_id in similarClusters) {
        //average scores
        testedClusterScore = similarClusters[testedCluster_id]/similarClustersRelsCount[targetNodeCluster_id];
            if (testedClusterScore > largestScore && this.clustering.positioned.indexOf(parseInt(testedCluster_id)) === -1) {
            largestScore = testedClusterScore;
            largestScoreCluster_id = testedCluster_id;
        }
    }
    
    return parseInt(largestScoreCluster_id);
};

Clustrovac.prototype.getLargestRemainingCluster = function() {
    var maxSize = 0;
    var clusterProductSize = 0;
    var clustersWithSize = 0;
    var cluster_id;
    var ci;
    for (ci = 0; ci < this.clustering.clusters.length; ci++) {
        var size = this.clustering.clusterSize[this.clustering.clusters[ci]];
        if (size > maxSize && this.clustering.positioned.indexOf(this.clustering.clusters[ci]) === -1) {
            cluster_id = this.clustering.clusters[ci];
            maxSize = size;
        }
        
        if (size > 0) {
            clustersWithSize++;
            clusterProductSize += size;
        }
    }
    
    console.log('clusters with size: '+clustersWithSize+', products in clusters: '+clusterProductSize);
    return cluster_id;
};

var EditorSkrini = function($targetElement, clustrovacData, clustrovac) {
    this.$targetElement = $targetElement;
    this.data = clustrovacData;
    this.clustrovac = clustrovac;    
        
    this.activeId = 0;
    this.activeType = 'pyramidy';
    this.activeToMove = [];
    
    this.initData();
    this.initHTML();
    this.initEventListeners();
};
EditorSkrini.prototype.initData = function() {
    console.log(this.data); 
    var i, j;
    //pyramidy
    for (i = 0; i < this.data.pyramidy.length; i++){
        var location = new Location(i, 0);
        for (j = 0; j < this.data.pyramidy[i].length; j++) {
            var id_product = this.data.pyramidy[i][j];
            var id_series = null;
            var productNode = this.clustrovac.data.product_node_reference[id_product];
            var nodeProductsLength = this.clustrovac.data.nodes[productNode].product_ids.length;
            if (nodeProductsLength > 1) {
                id_series = productNode;
            }
            
            var sales = this.clustrovac.data.nodes[productNode].product_metadata[id_product].sales;
            var locationBook = new LocationBook(id_product, id_series, 1, 0, sales);
            
            location.products.push(locationBook);
        }
        this.data.pyramidy[i] = location;
    }
    
    //skrine
    for (i = 0; i < this.data.skrine.length; i++) {
        var location = new Location(i, 1);
        for (j = 0; j < this.data.skrine[i].length; j++) {
            var id_product = this.data.skrine[i][j];
            var id_series = null;
            var productNode = this.clustrovac.data.product_node_reference[id_product];
            var nodeProductsLength = this.clustrovac.data.nodes[productNode].product_ids.length;
            if (nodeProductsLength > 1) {
                id_series = productNode;
            }
            var sales = this.clustrovac.data.nodes[productNode].product_metadata[id_product].sales;
            var locationBook = new LocationBook(id_product, id_series, 1, 0, sales);
            
            location.products.push(locationBook);
        }
        this.data.skrine[i] = location;
    }
    
    //limbo
    var location = new Location(0, 2);
    for (j = 0; j < this.data.limbo[0].length; j++) {
        var id_product = this.data.limbo[0][j];
        var id_series = null;
        var productNode = this.clustrovac.data.product_node_reference[id_product];
        var nodeProductsLength = this.clustrovac.data.nodes[productNode].product_ids.length;
        if (nodeProductsLength > 1) {
            id_series = productNode;
        }
        var sales = this.clustrovac.data.nodes[productNode].product_metadata[id_product].sales;
        var locationBook = new LocationBook(id_product, id_series, 1, 0, sales);

        location.products.push(locationBook);
    }
    this.data.limbo[0] = location;
};

EditorSkrini.prototype.initDataFromImport = function(json) {
    //var json = JSON.parse(jsonString);
    console.log(json); 
    this.data = {
        pyramidy: [],
        skrine: [],
        limbo: []
    };
    
    var i, j;
    //pyramidy
    for (i = 0; i < json.pyramidy.length; i++){
        var location = new Location(i, 0);
        location.name = json.pyramidy[i].name;
        for (j = 0; j < json.pyramidy[i].products.length; j++) {
            var product = json.pyramidy[i].products[j];
            var locationBook = new LocationBook(product.id_product, product.id_series, parseInt(product.active), parseInt(product.orientation), parseInt(product.sales));
            
            location.products.push(locationBook);
        }
        this.data.pyramidy.push(location);
    }
    
    //skrine
    for (i = 0; i < json.skrine.length; i++) {
        var location = new Location(i, 1);
        location.name = json.skrine[i].name;
        for (j = 0; j < json.skrine[i].products.length; j++) {
            var product = json.skrine[i].products[j];
            var locationBook = new LocationBook(product.id_product, product.id_series, parseInt(product.active), parseInt(product.orientation), parseInt(product.sales));
            
            location.products.push(locationBook);
        }
        this.data.skrine.push(location);
    }
    
    //limbo
    var location = new Location(0, 2);
    for (j = 0; j < json.limbo[0].products.length; j++) {
        var product = json.limbo[0].products[j];
        var locationBook = new LocationBook(product.id_product, product.id_series, parseInt(product.active), parseInt(product.orientation), parseInt(product.sales));

        location.products.push(locationBook);
    }
    this.data.limbo.push(location);
    
    $('#editorLeftMenu > table tr:gt(0)').remove();
    this.initHTML();
    this.renderActive();
};

EditorSkrini.prototype.initHTML = function() {
    for (i = 0; i < this.data.pyramidy.length; i++) {
        var location = this.data.pyramidy[i];
        var locationCapacityOverloadClass = (location.products.length > this.clustrovac.const.clFitPyramidSize) ? 'locationCapacityOverload' : '';
        var $tr = $("<tr class='locationMenuItem' id='loc-pyramidy"+location.id+"' data-locationType='pyramidy' data-locationid='"+location.id+"'></tr>");
        $tr.append('<td><button class="activateLocationBtn">></button></td>');
        $tr.append('<td class="locationMoveBtns"><button class="locationMoveUpBtn">&#8648;</button><button class="locationMoveDownBtn">&#8650;</button></td>');
        $tr.append('<td class="locationMoveable">'+location.id+'</td>');
        $tr.append('<td class="locationMoveable">P</td>');
        $tr.append('<td class="locationMoveable locationName">'+location.name+'</td>');
        $tr.append('<td class="locationMoveable locationCapacity '+locationCapacityOverloadClass+'">('+location.products.length+'/'+this.clustrovac.const.clFitPyramidSize+')</td>');
        $('#editorLeftMenu > table').append($tr);
        this.refreshCapacities('pyramidy', i);
    }
    
    for (i = 0; i < this.data.skrine.length; i++) {
        var location = this.data.skrine[i];
        var locationCapacityOverloadClass = (location.products.length > this.clustrovac.const.clFitDisplayWindowSize) ? 'locationCapacityOverload' : '';
        var $tr = $("<tr class='locationMenuItem' id='loc-skrine"+location.id+"' data-locationType='skrine' data-locationid='"+location.id+"'></tr>");
        $tr.append('<td><button class="activateLocationBtn">></button></td>');
        $tr.append('<td class="locationMoveBtns"><button class="locationMoveUpBtn">&#8648;</button><button class="locationMoveDownBtn">&#8650;</button></td>');
        $tr.append('<td class="locationMoveable">'+location.id+'</td>');
        $tr.append('<td class="locationMoveable">S</td>');
        $tr.append('<td class="locationMoveable locationName">'+location.name+'</td>');
        $tr.append('<td class="locationMoveable locationCapacity '+locationCapacityOverloadClass+'">('+location.products.length+'/'+this.clustrovac.const.clFitDisplayWindowSize+')</td>');
        $('#editorLeftMenu > table').append($tr);
        this.refreshCapacities('skrine', i);
    }
    
    var $tr = $("<tr class='locationMenuItem' id='loc-limbo0' data-locationType='limbo' data-locationid='0'></tr>");
    $tr.append('<td><button class="activateLocationBtn">></button></td>');
    $tr.append('<td class="locationMoveBtns"><button class="locationMoveUpBtn">&#8648;</button><button class="locationMoveDownBtn">&#8650;</button></td>');
    $tr.append('<td class="locationMoveable">0</td>');
    $tr.append('<td class="locationMoveable">L</td>');
    $tr.append('<td class="locationMoveable locationName">Limbo</td>');
    $tr.append('<td class="locationMoveable locationCapacity">()</td>');
    $('#editorLeftMenu > table').append($tr);
    this.refreshCapacities('limbo', 0);
};

EditorSkrini.prototype.initEventListeners = function() {
        //EVENT LISTENERS
    var that = this;
    $('#editorSeriiName').on('keyup', function() {
        var activeLocation = that.data[that.activeType][that.activeId];
        activeLocation.name = $(this).val();
        var elementId = '#loc-'+that.activeType+that.activeId;
        $(elementId).find('.locationName').html(activeLocation.name);
    });
    
    $("#editorLeftMenu").on('click', '.activateLocationBtn', function() {
        var targetType = $(this).parent().parent().attr('data-locationtype');
        var targetId = parseInt($(this).parent().parent().attr('data-locationid'));

        that.activeType = targetType;
        that.activeId = targetId;
        console.log('changed to targetType '+targetType+', targetId '+targetId);
        that.renderActive();
    });
    
    $('#editorLeftMenu').on('click', '.locationMoveable', function() {
        var targetType = $(this).parent().attr('data-locationtype');
        var targetId = parseInt($(this).parent().attr('data-locationid'));

        console.log('moving to targetType '+targetType+', targetId '+targetId);
        that.moveBooks(targetType, targetId);
    });
    
    $('#editorLocationView').on('click', '.changeLocationBtn', function() {
        var sourceType = that.activeType;
        var sourceId = that.activeId;
        var source = that.data[sourceType][sourceId];
        var id_product = parseInt($(this).parent().toggleClass('activeToMove').attr('data-idproduct'));
        var toMove = $(this).parent().hasClass('activeToMove');
       
        var i;
        for (i = 0; i < source.products.length; i++) {
            var product = source.products[i];
            if (product.id_product === id_product) {
                if (toMove) {
                    that.activeToMove.push(i);
                } else {
                    var index = that.activeToMove.indexOf(i);
                    if (index > -1) {
                        that.activeToMove.splice(index, 1);
                    }
                }
            }
        }
    });
    
    $('#editorLeftMenu').on('click', '.locationMoveUpBtn',function() {
        var targetType = $(this).parent().parent().attr('data-locationtype');
        var targetId = parseInt($(this).parent().parent().attr('data-locationid'));
        var dataObj = that.data[targetType][targetId];
        console.log(targetType, targetId);
        if (targetId === 0) {
            return;
        }
        
        var temp = that.data[targetType][targetId-1];
        that.data[targetType][targetId-1] = dataObj;
        that.data[targetType][targetId] = temp;

        var tempTr = $('#editorLeftMenu > table #loc-'+targetType+(targetId-1)).html();
        var targetTr = $('#editorLeftMenu > table #loc-'+targetType+(targetId)).html();
        $('#editorLeftMenu > table #loc-'+targetType+(targetId-1)).html('').append(targetTr).find('td:eq(2)').html(targetId-1);
        $('#editorLeftMenu > table #loc-'+targetType+(targetId)).html('').append(tempTr).find('td:eq(2)').html(targetId);
    });
    
    $('#editorLeftMenu').on('click', '.locationMoveDownBtn',function() {
        var targetType = $(this).parent().parent().attr('data-locationtype');
        var targetId = parseInt($(this).parent().parent().attr('data-locationid'));
        var dataObj = that.data[targetType][targetId];
        console.log(targetType, targetId);
        if (targetId === that.data[targetType].length-1) {
            return;
        }
        
        var temp = that.data[targetType][targetId+1];
        that.data[targetType][targetId+1] = dataObj;
        that.data[targetType][targetId] = temp;
        
        var tempTr = $('#editorLeftMenu > table #loc-'+targetType+(targetId+1)).html();
        var targetTr = $('#editorLeftMenu > table #loc-'+targetType+(targetId)).html();
        $('#editorLeftMenu > table #loc-'+targetType+(targetId+1)).html('').append(targetTr).find('td:eq(2)').html(targetId+1);
        $('#editorLeftMenu > table #loc-'+targetType+(targetId)).html('').append(tempTr).find('td:eq(2)').html(targetId);
    });
    
    $('#editorLocationContent').on('click', function() {
        that.refreshCapacities(that.activeType, that.activeId);
    });
    
    $('#editorSkriniExportBtn').on('click', function() {
        var url = URL.createObjectURL( new Blob( [JSON.stringify(that.data)], {type:'data:text/json;charset=utf-8'} ) );
        $("#downloadAnchorElem").attr("href", url)[0].click();
    });
    
    $('#editorSkriniImportBtn').on('click', function() {
        var file = $('#editorSkriniImportInput').val();
        $.getJSON("data/"+file, function(json) {
            that.initDataFromImport(json);
        });
    });
    
    $('#editorSkriniRedistributeLimboBtn').on('click', function() {
        that.redistributeLimbo();
    });
    
    $('#moveEntireActiveBtn').on('click', function() {
        $('#editorLocationContent').find('.locationBook .changeLocationBtn').trigger('click');
    });
    
    $('#editorSkriniExportSkrineAsListBtn').on('click', function() {
        that.exportIds(1);
    });
    $('#editorSkriniExportPyramidyAsListBtn').on('click', function() {
        that.exportIds(0);
    });
};

EditorSkrini.prototype.renderActive = function() {
    this.activeToMove = [];
    $('#editorLocationContentActive').html('');
    $('#editorLocationContentInactive').html('');
    
    var elementId = '#loc-'+this.activeType+this.activeId;
    $('#activeCapacity').html($(elementId).find('.locationCapacity').html());
    
    var activeLocation = this.data[this.activeType][this.activeId];
    $('#editorSeriiName').val(activeLocation.name);
    var i;
    for (i = 0; i < activeLocation.products.length; i++) {
        var product = activeLocation.products[i];
        product.toHTML();
    }
    
    if (activeLocation.type !== 1) {
        //neni skrin
        $('#editorLocationContentActive').addClass('noToggleOrientationBtn');
    } else {
        $('#editorLocationContentActive').removeClass('noToggleOrientationBtn');
    }
    
    this.refreshCapacities(this.activeType, this.activeId);
};

EditorSkrini.prototype.moveBooks = function(targetType, targetId) {
    var activeLocation = this.data[this.activeType][this.activeId];
    //sort desc
    this.activeToMove.sort(function(a, b){
        return b-a;
    });
    
    //you have to check if the article was activated inbetween
    var i;
    for (i = 0; i < this.activeToMove.length; i++) {
        var index = this.activeToMove[i];
        var locationBook = activeLocation.products[index];
        if (activeLocation.type !== 2) {
            //locationBook.active = 1;
        }
        this.data[targetType][targetId].products.push(locationBook);
        activeLocation.products.splice(index, 1);
    }
    
    $('#editorLocationView .series .activeToMove').parent().parent().remove();
    $('#editorLocationView .activeToMove').remove();
    this.activeToMove = [];
    
    this.refreshCapacities(this.activeType, this.activeId);
    this.refreshCapacities(targetType, targetId);
};

EditorSkrini.prototype.refreshCapacities = function(targetType, targetId) {
    var location = this.data[targetType][targetId];
    var elementId = '#loc-'+targetType+targetId;
    var maxCapacity, usedCapacity = 0, inactiveCapacity = 0;
    
    if (location.type === 0) {
        maxCapacity = this.clustrovac.const.clFitPyramidSize;
    } else if (location.type === 1) {
        maxCapacity = this.clustrovac.const.clFitDisplayWindowSize;
    } else if (location.type === 2) {
        maxCapacity = 9999;
    }
    
    //skrine
    if (location.type === 1) {
        var i;
        var celoProducts = 0;
        var hrbetProducts = 0;
        for (i = 0; i < location.products.length; i++) {
            var product = location.products[i];
            if (product.orientation === 0 && product.active === 1) {
                //celo
                celoProducts++;
            } else if (product.orientation === 1 && product.active === 1) {
                hrbetProducts++;
            } else {
                inactiveCapacity++;
            }
        }
        
        if (this.clustrovac.const.clFitDisplayWindowSize > celoProducts) {
            maxCapacity = celoProducts + (this.clustrovac.const.clFitDisplayWindowSize - celoProducts) * 4;
        }
        usedCapacity = celoProducts + hrbetProducts;
    } else {
        var i;
        for (i = 0; i < location.products.length; i++) {
            var product = location.products[i];
            if (product.active === 1) {
                usedCapacity++;
            } else {
                inactiveCapacity++;
            }
       }
    }
    
    $(elementId).find('.locationCapacity').html('('+usedCapacity+'/'+maxCapacity+', <span style="color: green">'+inactiveCapacity+'</span>)');

    if (usedCapacity > maxCapacity) {
        $(elementId).find('.locationCapacity').addClass('locationCapacityOverload');
    } else {
        $(elementId).find('.locationCapacity').removeClass('locationCapacityOverload');
    }
    
    if (targetType === this.activeType && targetId === this.activeId) {
        $('#activeCapacity').html($(elementId).find('.locationCapacity').html());
        
        if (location.type === 1) {
            $('#activeCapacity').append(' C:'+celoProducts+' H:'+hrbetProducts);
        }
    }
};

EditorSkrini.prototype.redistributeLimbo = function() {
    //create reference
    var reference = [];
    var j, k;
    for (j = 0; j < this.data.pyramidy.length; j++) {
        for (k = 0; k < this.data.pyramidy[j].products.length; k++) {
            var id_product = this.data.pyramidy[j].products[k].id_product;
            if (!this.clustrovac.data.product_node_reference[id_product]) {
                continue;
            }
            var productNode = this.clustrovac.data.nodes[this.clustrovac.data.product_node_reference[id_product]].id_node;
            if (!reference.hasOwnProperty(productNode)) {
                reference[productNode] = ['pyramidy_'+j];
            } else {
                reference[productNode].push('pyramidy_'+j);
            }
        }
    }
    for (j = 0; j < this.data.skrine.length; j++) {
        for (k = 0; k < this.data.skrine[j].products.length; k++) {
            var id_product = this.data.skrine[j].products[k].id_product;
            if (!this.clustrovac.data.product_node_reference[id_product]) {
                continue;
            }
            var productNode = this.clustrovac.data.nodes[this.clustrovac.data.product_node_reference[id_product]].id_node;
            if (!reference.hasOwnProperty(productNode)) {
                reference[productNode] = ['skrine_'+j];
            } else {
                reference[productNode].push('skrine_'+j);
            }
        }
    }
    console.log(reference);
    
    var i, l;
    for (i = this.data.limbo[0].products.length-1; i >= 0; i--) {
        var product = this.data.limbo[0].products[i];
        product.active = 0;
        var productNode = this.clustrovac.data.nodes[this.clustrovac.data.product_node_reference[product.id_product]];
        if (!productNode) {
            continue;
        }
        
        var id_node = productNode.id_node;
        var bestPlaces = [];
        var relations = productNode.relations;
        for (l = 0; l < relations.length; l++) {
            var relNode = relations[l];
            if (reference.hasOwnProperty(relNode)) {
                var key = reference[relNode];
                var keyStrength = this.clustrovac.data.node_relations_average[Math.min(id_node, relNode)+'_'+Math.max(id_node, relNode)];
                if (!bestPlaces.hasOwnProperty(id_node)) {
                    bestPlaces[key] = keyStrength;
                } else {
                    bestPlaces[key] += keyStrength;
                }
            }
        }
        
        var largestScore = 0;
        var largestScoreLocation = null;
        
        for (var key in bestPlaces) {
            var locScore = bestPlaces[key];
            if (locScore > largestScore) {
                largestScore = locScore;
                largestScoreLocation = key;
            }
        }
        
        if (largestScoreLocation) {
            var split = largestScoreLocation.split('_');
            var targetType = split[0];
            var targetId = parseInt(split[1]);
            //console.log(product.id_product, targetType, targetId);
            
            this.data[targetType][targetId].products.push(product);
            this.data.limbo[0].products.splice(i, 1);

            $('#editorLocationView .locationBook[data-idproduct='+product.id_product+']').parent().parent().remove();

            this.refreshCapacities('limbo', 0);
            this.refreshCapacities(targetType, targetId);
        }
    }
};

EditorSkrini.prototype.exportIds = function(type) {
    var product_ids = [];
    var filename = "";
    if (type === 0) {
        for (j = 0; j < this.data.pyramidy.length; j++) {
            for (k = 0; k < this.data.pyramidy[j].products.length; k++) {
                if (!this.data.pyramidy[j].products[k].active) {
                    continue;
                }
                var id_product = this.data.pyramidy[j].products[k].id_product;
                product_ids.push(id_product);
            }
        }
        filename = "pyramidy_product_ids.txt";
    } else if (type === 1) {
        for (j = 0; j < this.data.skrine.length; j++) {
            for (k = 0; k < this.data.skrine[j].products.length; k++) {
                if (!this.data.skrine[j].products[k].active) {
                    continue;
                }
                var id_product = this.data.skrine[j].products[k].id_product;
                product_ids.push(id_product);
            }
        }
        filename = "skine_product_ids.txt";
    }
    
    var productIdString = product_ids.join(',');
    
    var url = URL.createObjectURL( new Blob( [productIdString], {type:'data:text/plain;charset=utf-8'} ) );
    $("#downloadCSVAnchorElem").attr("download", filename);
    $("#downloadCSVAnchorElem").attr("href", url)[0].click();
};


var Location = function(id, type) {
    this.products = [];
    
    this.id = id;
    this.type = type;
    this.name = "";
    
    this.init();
};
Location.prototype.init = function() {
    if (this.type === 0) {
        this.name = 'Pyramida-'+this.id;
    } else if (this.type === 1) {
        this.name = 'Skrin-'+this.id;
    } else if (this.type === 2 ) {
        this.name = 'Limbo';
    } else {
        this.name = 'Jsem Chyba!';
    }
};

var LocationBook = function(id_product, id_series, active, orientation, sales) {
    this.id_product = id_product;
    this.id_series = id_series;
    this.active = active;
    this.orientation = orientation;
    this.name = id_product;
    this.sales = sales;
};

LocationBook.prototype.toHTML = function() {
    var $body = $('<div class="locationBook" data-idproduct="'+this.id_product+'"></div>');
    $body.append('<img src=https://img-cloud.megaknihy.cz/'+this.id_product+'-medium/0/img.jpg>');
    $body.append('<div>'+this.name+' ('+this.sales+')<div>');
    
    var $controls = $body.append('<div class="locationBookControls"><div>');
    if (this.active) {
        $controls.append('<button class="removeFromActiveBtn">X</button> <button class="changeLocationBtn">&#8644;</button> <button class="toggleOrientationBtn '+(this.orientation === 0 ? 'bookOrientationCelo' : 'bookOrientationHrbet')+'">'+(this.orientation === 0 ? 'C' : 'H')+'</button>')
    } else {
        $controls.append('<button class="changeLocationBtn">&#8644;</button> <button class="addToActiveBtn">&#8613;</button>')
    }
    
    //EVENT LISTENERS
    var that = this;
    $body.on('click', '.removeFromActiveBtn', function() {
        that.active = 0;
        var seriesContainer = null;
        if (that.id_series !== null) {
            seriesContainer = $(this).parent().parent().parent();
        }
        $(this).parent().remove();
        if (seriesContainer && $(seriesContainer).find('div').length === 0) {
            $(seriesContainer).remove();
        }
        that.toHTML();
    });
    $body.on('click', '.addToActiveBtn', function() {
        that.active = 1;
        var seriesContainer = null;
        if (that.id_series !== null) {
            seriesContainer = $(this).parent().parent().parent();
        }
        $(this).parent().remove();
        if (seriesContainer && $(seriesContainer).find('div').length === 0) {
            $(seriesContainer).remove();
        }
        that.toHTML();
    });
    $body.on('click', '.toggleOrientationBtn', function() {
        var displayString;
        if (that.orientation === 0) {
            that.orientation = 1;
            displayString = "H";
            $(this).removeClass("bookOrientationCelo");
            $(this).addClass("bookOrientationHrbet");
        } else {
            that.orientation = 0;
            displayString = "C";
            $(this).removeClass("bookOrientationHrbet");
            $(this).addClass("bookOrientationCelo");
        }
        $(this).html(displayString);
    });
    
    $body.append('<div><a href="https://megaknihy.cz/kategorie/'+this.id_product+'-kniha.html" target="_blank">(MK.cz)</div>')
    $body.append($controls);
    
    
    var destinationContainer = '#editorLocationContentActive';
    if (!this.active) { 
        destinationContainer = '#editorLocationContentInactive';
    }
    
    if (this.id_series !== null) {
        if ($(destinationContainer).find('.series-'+this.id_series).length === 0) {
            $series = $('<div class="series series-'+this.id_series+'">Serie '+this.id_series+' </div>');
            
            $seriesControls = $('<section class="controls"></section>');
            if (this.active) {
                $seriesControls.append('<button class="series-removeFromActiveBtn">X</button> <button class="series-changeLocationBtn">&#8644;</button>');
            } else {
                $seriesControls.append('<button class="series-changeLocationBtn">&#8644;</button> <button class="series-addToActiveBtn">&#8613;</button>');
            }
            
            $series.append($seriesControls);
            $series.append('<section class="content"></section>');
            
            //EVENT LISTENERS
            $($series).on('click', '.series-removeFromActiveBtn', function() {
                $(this).parent().parent().find('.locationBook .removeFromActiveBtn').trigger('click');
            });
            $($series).on('click', '.series-addToActiveBtn', function() {
                $(this).parent().parent().find('.locationBook .addToActiveBtn').trigger('click');
            });
            $($series).on('click', '.series-changeLocationBtn', function() {
                $(this).parent().parent().find('.locationBook .changeLocationBtn').trigger('click');
            });
            
            $(destinationContainer).append($series);
        }
        
        destinationContainer = destinationContainer+' > .series-'+this.id_series+' > .content';
        $(destinationContainer).append($body);
    } else {
        $(destinationContainer).prepend($body);
    }
};