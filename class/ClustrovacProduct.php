<?php
class ClustrovacProduct {
    public $id_product;
    public $id_supplier;
    public $id_category;
    public $sales;
    
    public function __construct($id_product, $id_category, $id_supplier, $sales) {
        $this->id_product  = $id_product;
        $this->id_supplier = $id_supplier;
        $this->id_category = $id_category;
        $this->sales       = $sales;
    }
}
