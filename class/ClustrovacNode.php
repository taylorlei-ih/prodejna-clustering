<?php
class ClustrovacNode {
    public $id_node          = array();
    public $product_ids      = array();
    public $product_metadata = array();
    public $relations        = array();
    public $total_sales      = 0;
    public $average_sales    = 0;
    
    public function __construct($id_node) {
        $this->id_node = $id_node;
    }
    
    public function calcAverageSales() {
        $this->average_sales = $this->total_sales / count($this->product_ids);
    }
}